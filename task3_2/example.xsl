<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes" omit-xml-declaration="yes"/>
    <xsl:key name="key1" match="@* | node()" use="name()"/>

    <xsl:template match="/">
        <result>
            <xsl:apply-templates select="(//@* | //*)[generate-id() = generate-id(key('key1',name())[1])]"/>
        </result>
    </xsl:template>

    <xsl:template match="@* | *">
        Node '<xsl:value-of select="name()"/>' found <xsl:value-of select="count(key('key1', name()))"/> times.
    </xsl:template>

</xsl:stylesheet>