<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes" omit-xml-declaration="yes"/>
    <xsl:variable name="upper" select="'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'"/>
    <xsl:variable name="lower" select="'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'"/>
    <xsl:key name="letters" match="list/item" use="translate(substring(@Name, 1, 1), $lower, $upper)"/>

    <xsl:template match="/">
        <directory>
            <xsl:call-template name="addNewLetter">
                <xsl:with-param name="alphabet" select="'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'"/>
            </xsl:call-template>
        </directory>
    </xsl:template>

    <xsl:template match="item">
        <name>
            <!--Put the value of the attribute "@Name" replacing the first char with a char in upper case-->
            <xsl:value-of select="concat(translate(substring(@Name, 1, 1), $lower, $upper), substring(@Name, 2))"/>
        </name>
    </xsl:template>

    <xsl:template name="addNewLetter">
        <xsl:param name="alphabet"/>
        <xsl:variable name="firstCharInAlphabet" select="substring($alphabet, 1, 1)"/>
        <xsl:choose>
            <xsl:when test="string-length($alphabet) &gt; 1">
                <xsl:if test="key('letters', $firstCharInAlphabet)">
                    <letter value="{substring($alphabet, 1, 1)}">
                        <xsl:apply-templates select="key('letters', $firstCharInAlphabet)">
                            <xsl:sort select="@Name"/>
                        </xsl:apply-templates>
                    </letter>
                </xsl:if>
                <xsl:call-template name="addNewLetter">
                    <xsl:with-param name="alphabet" select="translate($alphabet, $firstCharInAlphabet, '')"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="key('letters', $firstCharInAlphabet)">
                    <letter value="{$firstCharInAlphabet}">
                        <xsl:apply-templates select="key('letters', $firstCharInAlphabet)">
                            <xsl:sort select="@Name"/>
                        </xsl:apply-templates>
                    </letter>
                </xsl:if>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>