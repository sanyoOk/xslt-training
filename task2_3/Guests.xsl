<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="xml_version_1.0" xmlns:end="https://www.meme-arsenal.com/create/template/43024">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="*[local-name() = 'Guests' or local-name() = 'Guest']">
        <xsl:call-template name="renameElement"/>
    </xsl:template>

    <xsl:template match="@*">
        <xsl:call-template name="toUpperCase"/>
    </xsl:template>

    <xsl:template name="toUpperCase">
        <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'"/>
        <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
        <xsl:attribute name="{translate(name(), $lowercase, $uppercase)}">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="renameElement">
        <xsl:choose>
            <xsl:when
                    test="local-name() = 'Guests' and (namespace-uri() = 'https://www.meme-arsenal.com/create/template/43024')"> <!--Если эелемент Guests будет с префиксом end-->
                <end:Peoples>
                    <xsl:apply-templates select="@*|node()"/>
                </end:Peoples>
            </xsl:when>
            <xsl:when test="local-name() = 'Guests'"> <!--Если эелемент Guests будет без префикса end-->
                <Peoples>
                    <xsl:apply-templates select="@*|node()"/>
                </Peoples>
            </xsl:when>
            <xsl:when
                    test="(local-name() = 'Guest') and (namespace-uri() = 'https://www.meme-arsenal.com/create/template/43024')"> <!--Если эелемент Guest будет с префиксом end-->
                <end:People>
                    <xsl:apply-templates select="@*|node()"/>
                </end:People>
            </xsl:when>
            <xsl:otherwise>
                <People>
                    <xsl:apply-templates select="@*|node()"/>
                </People>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
</xsl:stylesheet>