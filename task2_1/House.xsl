<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:h="HouseChema" xmlns:i="HouseInfo" xmlns:r="RoomsChema">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:element name="AllRooms">
            <xsl:apply-templates select="h:Houses"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="h:Houses">
        <xsl:variable name="lowerCaser" select="'abcdefghijklmnopqrstuvwxyz'"/>
        <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

        <xsl:for-each
                select="h:House[boolean(normalize-space(@City))]"> <!--если у "House" аттрибут "City" отсутствует или пустой, то мы его игнорируем этот House -->
            <xsl:sort select="translate(@City, $lowerCaser, $upperCase)"/>
            <xsl:sort select="translate(i:Address, translate(i:Address,'0123456789', ''), '')"
                      data-type="number"/> <!--Будем считать, что номер дома может состоять только из цифр, а улица - из всего, кроме цифр-->
            <xsl:if test="count(descendant::r:Room) &gt; 0"> <!--Проверка на наличие комнат в доме как таковых -->
                <xsl:apply-templates select="h:Blocks"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="h:Blocks">
        <xsl:for-each
                select="h:Block[boolean(normalize-space(@number))]"> <!--если у "Block" аттрибут "number" отсутствует или пустой, то мы игнорируем этот Block -->
            <xsl:sort select="@number"/>
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="h:Block">
        <xsl:variable name="lowerCaser" select="'abcdefghijklmnopqrstuvwxyz'"/>
        <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

        <xsl:for-each select="descendant::r:Room">
            <xsl:sort select="@nuber" data-type="number"/>
            <xsl:element name="Room">
                <xsl:element name="Address">
                    <xsl:value-of select="concat(translate(ancestor::h:House/@City, $lowerCaser, $upperCase), '/')"/>
                    <xsl:value-of select="concat(ancestor::h:House/i:Address, '/')"/>
                    <xsl:value-of select="concat(ancestor::h:Block/@number, '/')"/>
                    <xsl:value-of select="concat(@nuber, '')"/>
                </xsl:element>
                <xsl:element name="HouseRoomsCount">
                    <xsl:value-of select="count(ancestor::h:House/descendant::r:Room)"/>
                </xsl:element>
                <xsl:element name="BlockRoomsCount">
                    <xsl:value-of select="count(ancestor::h:Block/descendant::r:Room)"/>
                </xsl:element>
                <xsl:element name="HouseGuestsCount">
                    <xsl:value-of select="sum(ancestor::h:House/descendant::r:Room/@guests)"/>
                </xsl:element>
                <xsl:element name="GuestsPerRoomIntoHouseAverage"> <!--Среднее кол-во гостей на одну комнату по дому-->
                    <xsl:value-of
                            select="floor((sum(ancestor::h:House/descendant::r:Room/@guests)) div (count(ancestor::h:House/descendant::r:Room)))"/>
                </xsl:element>
                <xsl:element
                        name="GuestsPerRoomIntoHousesAverage"> <!--Среднее кол-во гостей на одну комнату по всем домам-->
                    <xsl:value-of
                            select="floor((sum(ancestor::h:Houses/descendant::r:Room/@guests)) div (count(ancestor::h:Houses/descendant::r:Room)))"/>
                </xsl:element>
                <xsl:element name="Allocated">
                    <xsl:attribute name="Single">
                        <xsl:value-of select="string(number(@guests) = 1)"/>
                    </xsl:attribute>
                    <xsl:attribute name="Double">
                        <xsl:value-of select="string(number(@guests) = 2)"/>
                    </xsl:attribute>
                    <xsl:attribute name="Triple">
                        <xsl:value-of select="string(number(@guests) = 3)"/>
                    </xsl:attribute>
                    <xsl:attribute name="Quarter">
                        <xsl:value-of select="string(number(@guests) = 4)"/>
                    </xsl:attribute>
                </xsl:element>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>