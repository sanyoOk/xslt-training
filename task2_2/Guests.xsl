<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns="xml_version_1.0" xmlns:end="https://www.meme-arsenal.com/create/template/43024">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="*[local-name() = 'Guests']">
        <Guests>
            <xsl:apply-templates select="*[local-name() = 'Guest']"/>
        </Guests>
    </xsl:template>
    
    <xsl:template match="*[local-name() = 'Guest']">
        <xsl:value-of select="concat(translate(name(), '&#x9;&#xa;&#xd; &#x20;', ''), '/')"/>
        <xsl:value-of select="concat(*[local-name() = 'Type'], '/')"/>
        <xsl:value-of select="concat(@Age, '/')"/>
        <xsl:value-of select="concat(@Nationalty, '/')"/>
        <xsl:value-of select="concat(@Gender, '/')"/>
        <xsl:value-of select="concat(@Name, '#')"/>
    </xsl:template>
</xsl:stylesheet>