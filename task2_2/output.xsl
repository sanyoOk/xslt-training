<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*[local-name() = 'Guests']">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:variable name="inputStr" select="."/>
            <xsl:if test="string-length($inputStr) &gt; 0">
                <xsl:call-template name="splitInputGuests">
                    <xsl:with-param name="inputGuests" select="$inputStr"/>
                    <xsl:with-param name="delimiter" select="'#'"/>
                </xsl:call-template>
            </xsl:if>
        </xsl:copy>
    </xsl:template>

    <xsl:template name="splitInputGuests">
        <xsl:param name="inputGuests"/>
        <xsl:param name="delimiter"/>
        <xsl:choose>
            <xsl:when test="contains($inputGuests, $delimiter) and contains($inputGuests, '/')">
                <xsl:element name="{normalize-space(substring-before($inputGuests, '/'))}" namespace="{namespace-uri()}">
                    <xsl:call-template name="splitGuest">
                        <xsl:with-param name="inputGuest" select="substring-after(substring-before($inputGuests,$delimiter), '/')"/>
                        <xsl:with-param name="delimiter" select="'/'"/>
                        <xsl:with-param name="index" select="1"/>
                    </xsl:call-template>
                </xsl:element>
                <xsl:call-template name="splitInputGuests">
                    <xsl:with-param name="inputGuests" select="substring-after($inputGuests, $delimiter)"/>
                    <xsl:with-param name="delimiter" select="$delimiter"/>
                </xsl:call-template>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="splitGuest">
        <xsl:param name="inputGuest"/>
        <xsl:param name="delimiter"/>
        <xsl:param name="index"/>
        <xsl:choose>
            <xsl:when test="(contains($inputGuest, $delimiter)) and ($index &lt; 2)">
                <xsl:call-template name="splitGuest">
                    <xsl:with-param name="inputGuest" select="substring-after($inputGuest, $delimiter)"/>
                    <xsl:with-param name="delimiter" select="$delimiter"/>
                    <xsl:with-param name="index" select="$index+1"/>
                </xsl:call-template>
                <xsl:call-template name="addNewElementForGuest">
                    <xsl:with-param name="index" select="$index"/>
                    <xsl:with-param name="elementValue" select="substring-before($inputGuest, $delimiter)"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="(contains($inputGuest, $delimiter)) and ($index &lt; 5)">
                <xsl:call-template name="addAttributeToTheTourist">
                    <xsl:with-param name="index" select="$index"/>
                    <xsl:with-param name="attrValue" select="substring-before($inputGuest, $delimiter)"/>
                </xsl:call-template>
                <xsl:call-template name="splitGuest">
                    <xsl:with-param name="inputGuest" select="substring-after($inputGuest, $delimiter)"/>
                    <xsl:with-param name="delimiter" select="$delimiter"/>
                    <xsl:with-param name="index" select="$index+1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="addAttributeToTheTourist">
                    <xsl:with-param name="index" select="$index"/>
                    <xsl:with-param name="attrValue" select="$inputGuest"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="addAttributeToTheTourist">
        <xsl:param name="attrValue"/>
        <xsl:param name="index"/>
        <xsl:choose>
            <xsl:when test="$index = 2">
                <xsl:attribute name="Age">
                    <xsl:value-of select="$attrValue"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="$index = 3">
                <xsl:attribute name="Nationalty">
                    <xsl:value-of select="$attrValue"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="$index = 4">
                <xsl:attribute name="Gender">
                    <xsl:value-of select="$attrValue"/>
                </xsl:attribute>
            </xsl:when>
            <xsl:when test="$index = 5">
                <xsl:attribute name="Name">
                    <xsl:value-of select="$attrValue"/>
                </xsl:attribute>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="addNewElementForGuest">
        <xsl:param name="index"/>
        <xsl:param name="elementValue"/>
        <xsl:if test="$index = 1">
            <xsl:element name="Type" namespace="{namespace-uri()}">
                <xsl:value-of select="$elementValue"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>