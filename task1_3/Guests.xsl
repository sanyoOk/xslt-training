<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes"/>

    <xsl:template match="*[local-name() = 'Guests']">
        <xsl:element name="guests">
            <xsl:apply-templates select="*[local-name() = 'Guest']"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*[local-name() = 'Guest']">
        <xsl:value-of select="concat(*[local-name() = 'Type'], '/')"/>
        <xsl:value-of select="concat(@Age, '/')"/>
        <xsl:value-of select="concat(@Nationalty, '/')"/>
        <xsl:value-of select="concat(@Gender, '/')"/>
        <xsl:value-of select="concat(@Name, '#')"/>
    </xsl:template>


</xsl:stylesheet>