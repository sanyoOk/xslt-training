<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" encoding="UTF-8" version="1.0" indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="*[local-name() = 'Guests']">
        <xsl:element name="result">
            <xsl:apply-templates select="*[local-name() = 'Guest'][@Name = 'Jimmy']"/>
            <xsl:call-template name="addressesWithoutPushkinskaya"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="*[local-name() = 'Guest']">
        <xsl:for-each select="preceding-sibling::*[local-name() = 'Guest']">
            <xsl:call-template name="toLowerCase">
                    <xsl:with-param name="str" select="attribute::Name"/>
            </xsl:call-template>
        </xsl:for-each>

        <xsl:value-of select="concat('&#xA;', '___________________________', '&#xA;')"/>

        <xsl:for-each select="following-sibling::*[(local-name() = 'Guest') and (@Nationalty != 'BY') ]/*[local-name() = 'Profile']">
            <xsl:value-of select="concat('&#xA;',descendant::*[local-name() = 'Address'])"/>
        </xsl:for-each>

        <xsl:value-of select="concat('&#xA;', '___________________________', '&#xA;')"/>
    </xsl:template>

    <xsl:template name="addressesWithoutPushkinskaya">
        <xsl:for-each select="descendant::*[local-name() = 'Profile']/*[(local-name() = 'Address') and (not(contains(text(), 'Pushkinskaya')))]">
            <xsl:element name="Address">
                <xsl:copy-of select="../../@Name"/>
                <xsl:copy-of select="../../@Nationalty"/>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="toLowerCase">
        <xsl:param name="str"/>
        <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
        <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
        <xsl:value-of select="concat('&#xA;',translate($str, $uppercase, $lowercase))"/>
    </xsl:template>

</xsl:stylesheet>